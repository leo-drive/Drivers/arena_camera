#include "arena_camera/arena_camera_node.h"
#include "arena_camera/camera_settings.h"

#include <rcutils/logging_macros.h>
#include <rclcpp_components/register_node_macro.hpp>

#include <signal.h>


ArenaCameraNode::ArenaCameraNode(const rclcpp::NodeOptions &node_options)
        : rclcpp::Node{
        "arena_camera_node", node_options} {
    m_arena_camera_handler = std::make_unique<ArenaCamerasHandler>();
    auto camera_settings = read_camera_settings();
    m_arena_camera_handler->create_cameras_from_settings(camera_settings);
    m_publishers = create_publishers(
            this,
            m_arena_camera_handler->get_camera_count());
    m_arena_camera_handler->set_image_callback(
            std::bind(
                    &ArenaCameraNode::publish_image, this,
                    std::placeholders::_1,
                    std::placeholders::_2));
    m_arena_camera_handler->start_stream();
}

std::vector<CameraSetting> ArenaCameraNode::read_camera_settings() {
    std::vector<CameraSetting> camera_setting;
    const std::string cameras_param_name{"camera_names"};
    const auto camera_names{declare_parameter(cameras_param_name, std::vector<std::string>{})};
    RCLCPP_INFO(get_logger(), "Cameras size : %d", camera_names.size());
    for (std::size_t i = 0; i < camera_names.size(); i++) {
        std::string prefix = camera_names.at(i) + ".";
        camera_setting.emplace_back(
                camera_names.at(i),
                declare_parameter(prefix + "frame_id").template get<std::string>(),
                declare_parameter(prefix + "pixel_format").template get<std::string>(),
                declare_parameter(prefix + "serial_no").template get<uint32_t>(),
                declare_parameter(prefix + "fps").template get<uint32_t>(),
                declare_parameter(prefix + "width").template get<uint32_t>(),
                declare_parameter(prefix + "height").template get<uint32_t>(),
                declare_parameter(prefix + "flip_enable").template get<bool>()
        );
    }
    return camera_setting;
}

std::vector<ArenaCameraNode::ProtectedPublisher>
ArenaCameraNode::create_publishers(
        ::rclcpp::Node *node,
        const size_t number_of_cameras) {
    if (!node) {
        throw std::runtime_error("The node is not initialized. Cannot create publishers.");
    }
    const auto number_of_publishers = number_of_cameras;
    std::vector<ProtectedPublisher> publishers(number_of_publishers);
    for (auto i = 0U; i < number_of_publishers; ++i) {
        publishers[i].set_publisher(
                node->create_publisher<sensor_msgs::msg::Image>(create_camera_topic_name(i), 10));
    }
    return publishers;
}

void ArenaCameraNode::publish_image(
        std::uint32_t camera_index,
        std::unique_ptr<sensor_msgs::msg::Image> image) {
    const auto publisher_index = camera_index;
    if (image) {
        try{
            image->header.stamp = this->now();
        }catch(...){
            throw std::runtime_error("Problem is here....");
        }
        m_publishers.at(publisher_index).publish(std::move(image));
    }
}

void ArenaCameraNode::ProtectedPublisher::set_publisher(PublisherT::SharedPtr publisher) {
    m_publisher = publisher;
}

void ArenaCameraNode::ProtectedPublisher::publish(
        std::unique_ptr<sensor_msgs::msg::Image> image) {
    if (m_publisher) {
        const std::lock_guard<std::mutex> lock{m_publish_mutex};
        if (image) {
            m_publisher->publish(std::move(image));
        }
    } else {
        throw std::runtime_error("Publisher is nullptr, cannot publish.");
    }
}
RCLCPP_COMPONENTS_REGISTER_NODE(ArenaCameraNode)