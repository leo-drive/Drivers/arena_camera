#ifndef BUILD_SRC_ARENA_CAMERA_SRC_ARENA_CAMERA_NODE_H_
#define BUILD_SRC_ARENA_CAMERA_SRC_ARENA_CAMERA_NODE_H_

#include <rclcpp/rclcpp.hpp>
#include <rclcpp/publisher.hpp>

#include <sensor_msgs/msg/image.hpp>

#include <Arena/ArenaApi.h>

#include <chrono>
#include <thread>

#include "arena_camera/camera_settings.h"
#include "arena_cameras_handler.h"

class ArenaCameraNode: public ::rclcpp::Node {
public:
  explicit ArenaCameraNode(const rclcpp::NodeOptions & node_options);

  std::vector < CameraSetting > read_camera_settings();

private:
  class ProtectedPublisher;

  void publish_image(
    std::uint32_t camera_index,
    std::unique_ptr < sensor_msgs::msg::Image > image);

  static std::vector < ProtectedPublisher > create_publishers(
    ::rclcpp::Node * node,
    size_t number_of_cameras);

  static std::string create_camera_topic_name(std::uint32_t camera_index)
  {
    return "/lucid_vision/camera_" + std::to_string(camera_index) + "/image_raw";
  }

  std::unique_ptr < ArenaCamerasHandler > m_arena_camera_handler;

  std::vector < ProtectedPublisher > m_publishers {};
};

class ArenaCameraNode::ProtectedPublisher
{
    using PublisherT = ::rclcpp::Publisher<::sensor_msgs::msg::Image>;

public:

    void set_publisher(PublisherT::SharedPtr publisher);
    void publish(std::unique_ptr<sensor_msgs::msg::Image> image);

private:
    std::mutex m_publish_mutex{};
    PublisherT::SharedPtr m_publisher{};
};


#endif //BUILD_SRC_ARENA_CAMERA_SRC_ARENA_CAMERA_NODE_H_
