#ifndef BUILD_ARENA_CAMERA_H
#define BUILD_ARENA_CAMERA_H

#include "arena_camera/camera_settings.h"
#include "Arena/ArenaApi.h"
#include <opencv2/opencv.hpp>
#include <sensor_msgs/msg/image.hpp>

#include <string>
#include <iostream>
#include <future>
#include <thread>


class ArenaCamera final {
public:
  explicit ArenaCamera(
    Arena::IDevice * device,
    CameraSetting & camera_setting,
    uint32_t camera_idx);

  ArenaCamera(
    Arena::IDevice * device,
    std::string & camera_name,
    std::string & frame_id,
    std::string & pixel_format,
    uint32_t serial_no,
    uint32_t fps,
    uint32_t camera_idx,
    uint32_t width,
    uint32_t height,
    bool flip_enable);

  ~ArenaCamera();

  std::thread start_stream();

  void stop_stream();

  void destroy_device(Arena::ISystem* system);

  void acquisition();

  std::unique_ptr<sensor_msgs::msg::Image> convert_to_image_msg(
            Arena::IImage * pImage, const std::string & frame_id);

  using ImageCallbackFunction = std::function<void(
          std::uint32_t,
          std::unique_ptr<sensor_msgs::msg::Image>)>;

  ImageCallbackFunction m_signal_publish_image{};

  void set_on_image_callback(ImageCallbackFunction callback);

private:
  Arena::IDevice * m_device;

  std::string m_camera_name;

  std::string m_frame_id;

  std::string m_pixel_format;

  uint32_t m_serial_no;

  uint32_t m_fps;

  uint32_t m_cam_idx;

  uint32_t m_width;

  uint32_t m_height;

  bool m_flip_enable;

  std::shared_future<void> future_;

  bool m_continue_acquiring{false};

  Arena::IImage* pImage = nullptr;




};


#endif //BUILD_ARENA_CAMERA_H
